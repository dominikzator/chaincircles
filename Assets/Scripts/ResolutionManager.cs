﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionManager : MonoBehaviour {

	public GameObject LeftBorder,RightBorder,UpperBorder,LowerBorder;
	public bool resolutionLogs;
	public float screenFactor, screenUnit;
	public float startingX;

	private float screenWidthPixels,screenHeightPixels,screenWidthUnits,screenHeightUnits;
	private float leftBorderX, rightBorderX;

	// Use this for initialization
	void Start () {

		screenWidthPixels = Screen.width;
		screenHeightPixels = Screen.height;

		screenFactor = screenWidthPixels / screenHeightPixels;
		screenUnit = screenFactor * 20 / 50;
		startingX = -10f * screenFactor;
		screenHeightUnits = Camera.main.orthographicSize*2;
		screenWidthUnits = screenHeightUnits * screenFactor;


		leftBorderX = -1 * ((screenWidthUnits + 15) / 2);
		rightBorderX = (screenWidthUnits + 15) / 2;

		//LeftBorder.transform.position = new Vector3 (leftBorderX, 8f, 0f);
		//RightBorder.transform.position = new Vector3 (rightBorderX, 8f, 0f);
		if (LeftBorder && RightBorder) {
			LeftBorder.transform.position = new Vector3 (-20.55f, 8f, 0f);
			RightBorder.transform.position = new Vector3 (20.55f, 8f, 0f);
		}



		if (resolutionLogs) {
			Debug.Log ("Screen width pixels: " + screenWidthPixels);
			Debug.Log ("Screen height pixels: " + screenHeightPixels);
			Debug.Log ("Screen factor: " + screenFactor);

			Debug.Log ("Screen width units: " + screenWidthUnits);
			Debug.Log ("Screen height units: " + screenHeightUnits);

			Debug.Log ("LeftBorderX: " + leftBorderX); 
			Debug.Log ("RightBorderX: " + rightBorderX); 
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
