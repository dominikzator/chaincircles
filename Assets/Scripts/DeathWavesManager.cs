﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathWavesManager : MonoBehaviour {

	public List<GameObject> deathWaves;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public GameObject GetFreeWave()
	{
		for (int i = 0; i < deathWaves.Count; i++) {
			if (deathWaves [i] != null) {
				return deathWaves [i];
			}
		}


		return null;
	}
}
