﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlusOneText : MonoBehaviour {

	public int buffor;
	public Animator anim;

	private Text txt;
	private GameObject multiplierText;

	// Use this for initialization
	void Start () {
		buffor = 0;
		txt = gameObject.GetComponent <Text>() ;
		anim = gameObject.GetComponent<Animator> ();
		multiplierText = GameObject.FindGameObjectWithTag("Multiplier Text");
		//txt.text = "+10";
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void IncrementText()
	{
		buffor++;
		txt.text = "+" + buffor.ToString ();
		txt.color = new Color (txt.color.r,txt.color.g,txt.color.b,1f);
		//Debug.Log ("Increment");
	}

	public void MultiplierSwipe()
	{
		anim.SetTrigger ("slideTrigger");
		//Debug.Log ("TEST");
	}

	public void ResetMultiplierText()
	{
		txt.text = "";
		buffor = 0;
		//Debug.Log ("Reset");
	}

	public void SetMultiplierText()
	{
		multiplierText.GetComponent<Text> ().text = "x"+Ball.multiplier.ToString();
	}

	public void DestroyIt()
	{
		Destroy (gameObject);
	}
}
