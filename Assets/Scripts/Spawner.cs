﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

	public ResolutionManager res;
	public Ball ball;
	public GameObject circleCanvas,circles,circle, ballPrefab, plusOneText;
	public Text waveText, multiplierText;
	public bool spawnerSet = true;
	public static int waveNumber = 0;
	public float circlesProbability, ballsProbability, probabilityCircleFactor;
	public bool spawnerWorking = false;

	public float circlesHeight;

	private MechanicsManager mechanicsManager;
	private LowerBorder lowerBorder;
	private int circlesInRow = 12;


	private float rowspeed = 2f;
	private BallLauncher ballLauncher;
	private List<int> strongerCirclesIndex;

	// Use this for initializations
	public void Start () {
		mechanicsManager = GameObject.FindGameObjectWithTag ("Mechanics Manager").GetComponent<MechanicsManager>();
		lowerBorder = GameObject.FindObjectOfType<LowerBorder> ();
		ballLauncher = GameObject.FindObjectOfType<BallLauncher> ();
		circlesHeight = circles.gameObject.transform.localPosition.y;
		spawnerWorking = false;
		//SpawnSomeFullRows (2);
		//Debug.Log("Spawner Awake");
		CancelInvoke ();
		InvokeRepeating("SpawnerTick",0f,1f);
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log ("SpawnerWorking: " + spawnerWorking);
		if (spawnerWorking) {
			MoveAllCircles ();
		}
	}

	void SpawnerTick()
	{
		//Debug.Log ("First Ball: " + lowerBorder.firstBall);
		//Debug.Log ("returned: " + lowerBorder.firstBall.GetComponent<Ball> ().returned);
		//Debug.Log ("Spawnerworking " + spawnerWorking);
		//Debug.Log ("Launch balll number: " + BallLauncher.launchBallsNumber);
		//Debug.Log ("Flying Balls number: " + BallLauncher.flyingBalls.Count);
		//Debug.Log ("Waves number: " + mechanicsManager.waves.Count);
		//Debug.Log ("////////////////////////////////");
		if (lowerBorder.firstBall 
			&& lowerBorder.firstBall.GetComponent<Ball>().returned == true
			&& !spawnerWorking 
			&& BallLauncher.launchBallsNumber == 1
			&& BallLauncher.flyingBalls.Count == 0){
			//Debug.Log ("IF");
			//GameObject[] waves = GameObject.FindGameObjectsWithTag ("DeathWave");

			if(!mechanicsManager.waves.Exists(x =>x) )
			{
				if (mechanicsManager.speedingUp) {
					mechanicsManager.SetTimeScale ();
				}
				mechanicsManager.speedUpButton.SetActive (false);
				mechanicsManager.startValuesTaken = false;
				lowerBorder.firstBall.GetComponent<Ball>().returned = false;
				spawnerWorking = true;
				circlesHeight -= 1.25f;
				SpawnNextRow ();
				mechanicsManager.waves.Clear ();
			}
		} 
	}

	bool CheckIfAllBallsReturned()
	{
		Ball[] ballsScripts = FindObjectsOfType<Ball> ();
		for (int i = 0; i < ballsScripts.Length; i++) {
			if (ballsScripts [i].returned == false) {
				return false;
			}
		}
		return true;
	}

	void MoveAllCircles ()
	{
		circles.transform.position -= new Vector3 (0f, rowspeed * Time.deltaTime, 0f);
		if (circles.transform.position.y < circlesHeight) {
			circles.transform.position = new Vector3 (circles.transform.position.x, circlesHeight, circles.transform.position.z);
			spawnerWorking = false;
			spawnerSet = true;
			ballLauncher.dragFlag = true;
		}
	}

	bool CheckIfLastItemDuplicates(List<int> input)
	{
		if (input.Count == 1) {
			return false;
		}
		for (int i = 0; i < input.Count-1; i++) {
			if (input [input.Count - 1] == input [i]) {
				return true;
			}
		}

		return false;
	}

	bool PlaceForABigOne (int input)
	{
		for (int i = 0; i < strongerCirclesIndex.Count; i++) {
			if (input == strongerCirclesIndex [i]) {
				return true;
			}
		}

		return false;
	}

	void UpdateCirclesProbability()
	{
		if (waveNumber > 40 && waveNumber <= 140) {
			circlesProbability = 40f + (waveNumber - 40f) * 0.3f;
		} else if (waveNumber > 140 && waveNumber <= 200) {
			circlesProbability = 70f + (waveNumber - 140f) * 0.5f;
		}

		if (waveNumber <= 100) {
			probabilityCircleFactor = 2f;
		} else if (waveNumber > 100 && waveNumber <= 200) {
			probabilityCircleFactor = 2 - (waveNumber - 100) * 0.01f;	
		}
	}

	void SpawnRandomRow(float probabilityCircle, float probabilityExtraBall)
	{
		waveNumber++;
		plusOneText.GetComponent<PlusOneText>().MultiplierSwipe ();
		UpdateCirclesProbability ();
		waveText.text = waveNumber.ToString ();
		if (MechanicsManager.interfaceAnimationsON) {
			waveText.GetComponent<Animator> ().SetTrigger ("Increment Trigger");
		}
		int ballSpawnIndex = Random.Range(0,11);
		strongerCirclesIndex = new List<int>();

		if (waveNumber >= 20 && waveNumber%10 == 0) {
			int strongCirclesFrequency = (waveNumber / 10)-1;
			if (strongCirclesFrequency > 12) {
				strongCirclesFrequency = 12;
			}
			for (int i = 0; i < strongCirclesFrequency; i++) {
				strongerCirclesIndex.Add (Random.Range (0, 12));
				if(CheckIfLastItemDuplicates (strongerCirclesIndex))
				{
					strongerCirclesIndex.Remove (strongerCirclesIndex[strongerCirclesIndex.Count-1]);
					i--;
				}
			}

		}

		for (int i = 0; i < circlesInRow; i++) 
		{
			int randomNumber = Random.Range (0, 100);

			if (PlaceForABigOne (i)) {
				GameObject cir = Instantiate (circle, new Vector3 (-5f + i*0.9f, 18.75f-circles.transform.position.y, 0f), Quaternion.identity) as GameObject;				
				cir.transform.SetParent (circles.transform, false);
				cir.GetComponentInChildren<Circle> ().SetHP (2*waveNumber);
			}
			else if (i == ballSpawnIndex) {			//Spawn Extra Ball
				GameObject extra = Instantiate (ballPrefab, new Vector3 (-5f + i*0.9f, 18.75f-circles.transform.position.y, 0f), Quaternion.identity) as GameObject;				
				extra.transform.SetParent (circles.transform, false);
				extra.transform.SetParent (circles.transform, false);
				extra.GetComponent<CircleCollider2D> ().isTrigger = true;
			}
			else if (randomNumber < probabilityCircle / probabilityCircleFactor) {	//Spawn wave number hp circles
				GameObject cir = Instantiate (circle, new Vector3 (-5f + i*0.9f, 18.75f-circles.transform.position.y, 0f), Quaternion.identity) as GameObject;				
				cir.transform.SetParent (circles.transform, false);
				cir.GetComponentInChildren<Circle> ().SetHP (waveNumber);
			} else if (randomNumber >= probabilityCircle / probabilityCircleFactor && randomNumber < probabilityCircle) {		//Spawn random Circles
				GameObject cir = Instantiate (circle, new Vector3 (-5f + i*0.9f, 18.75f-circles.transform.position.y, 0f), Quaternion.identity) as GameObject;				
				cir.transform.SetParent (circles.transform, false);
				int randomHp = Random.Range (1, waveNumber);
				cir.GetComponentInChildren<Circle> ().SetHP (randomHp);
			}
		}
	}

	void SpawnSomeFullRows(int number)
	{
		for (int j = 0; j < number; j++) {
			for (int i = 0; i < circlesInRow; i++) {
				GameObject cir = Instantiate (circle, new Vector3 (-5f + i*0.9f, 17.5f-j*1.25f, 0f), Quaternion.identity) as GameObject;
				cir.transform.SetParent (circles.transform, false);
				int randomHp = Random.Range (1, waveNumber);
				cir.GetComponentInChildren<Circle> ().SetHP (randomHp);
			}
		}
	}

	void SpawnNextRow()
	{
		SpawnRandomRow (circlesProbability,ballsProbability);
	}
}
