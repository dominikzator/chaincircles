﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {

	public static bool readyOnStart = false;
	public static bool freezeX = false;	//if true, after launch, ball moves vertcally(made for testing)
	public static int multiplier = 1;
	private static Text multiplierText;

	public Spawner spawner;
	public BallLauncher ballLauncher;
	public bool returned;
	public float killFlagSpeed; 

	private Rigidbody2D rg;
	private LowerBorder lowerBorder;
	private float firstBallX, difference;
	private bool killFlag;

	private MechanicsManager mechanicsManager;

	void Awake()
	{
		//Debug.Log ("Awake function");
		returned = false;
		killFlag = false;
		mechanicsManager = FindObjectOfType<MechanicsManager> ();
		lowerBorder = GameObject.FindObjectOfType<LowerBorder> ();
		multiplierText = GameObject.FindGameObjectWithTag ("Multiplier Text").GetComponent<Text>();
		rg = GetComponent<Rigidbody2D> ();
		SetGravity (0.08f);
	}

	// Use this for initialization
	void Start () {
		//Debug.Log ("Start function");

	}
	
	// Update is called once per frame
	void Update () {
			if (!readyOnStart && transform.position.y <= 0.5f) {
			SetStartingValues ();
			}
		//Time.timeScale = 0;
		//Debug.Log (readyOnStart);
	}

	void FixedUpdate()
	{
		if (gameObject.tag == "Launch Ball") {
			if (!readyOnStart && transform.position.y > 0.5f) {
				transform.position += new Vector3 (0f, -0.55f * Time.deltaTime, 0f);
			}

			if (killFlag) {
				if (difference > 0) {
					transform.position -= new Vector3 (killFlagSpeed * Time.deltaTime, 0f, 0f);
					if (transform.position.x < firstBallX) {
						BallLauncher.launchBallsNumber--;
						gameObject.transform.position = lowerBorder.firstBall.transform.position;
						killFlag = false;
						//Destroy (gameObject);
					}
				} else if (difference < 0) {
					transform.position += new Vector3 (killFlagSpeed * Time.deltaTime, 0f, 0f);
					if (transform.position.x > firstBallX) {
						BallLauncher.launchBallsNumber--;
						gameObject.transform.position = lowerBorder.firstBall.transform.position;
						killFlag = false;
						//Destroy (gameObject);
					}
				} else {
					BallLauncher.launchBallsNumber--;
					gameObject.transform.position = lowerBorder.firstBall.transform.position;
					killFlag = false;
					//Destroy (gameObject);
				}
			}
		}
	}

	public void GravityOff()
	{
		rg.gravityScale = 0f;
	}

	public void SetGravity(float value)
	{
		rg.gravityScale = value;
	}

	public void IncrementMultiplier()
	{
		multiplier++;
		multiplierText.GetComponent<Text>().text = "x" + multiplier.ToString ();
	}

	public static void DecrementMultiplier()
	{
		multiplier--;
		multiplierText.GetComponent<Text>().text = "x" + multiplier.ToString ();
	}

	public static void SetMultiplier(int input)
	{
		multiplier = input;
		multiplierText.GetComponent<Text> ().text = "x" + multiplier.ToString();
	}


	public void DestroyBall()
	{
		Destroy (gameObject);
	}

	public void QuickLaunch()
	{
		Ball ball = GameObject.FindGameObjectWithTag("Launch Ball").GetComponent<Ball>();
		ball.returned = true;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Flying Ball") {

			multiplier++;
			//Instantiate (plusOneText,MechanicsManager.circlesCanvas.transform);

			mechanicsManager.plusOneText.GetComponent<PlusOneText>().IncrementText();

			DestroyBall ();
		}
		if (col.gameObject.tag == "Trigger Border") {
			if (gameObject.tag == "Bonus Ball") {
				Destroy (gameObject);
			}
		}

	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.collider.tag == "Lower Border") {

			int randomInt = Random.Range (0, 100);

			if (randomInt < 100 - MechanicsManager.bounceProbability) {
				
				BallLauncher.flyingBalls.Remove (this.gameObject);
				if (lowerBorder.firstBall == null) {
					lowerBorder.firstBall = gameObject;
				} else if (lowerBorder.firstBall) {
					firstBallX = lowerBorder.firstBall.transform.position.x;
					difference = gameObject.transform.position.x - firstBallX;
					killFlag = true;
				}
				
				rg.bodyType = RigidbodyType2D.Static;
				transform.position = new Vector3 (transform.position.x, 0.5f, transform.position.z);
				returned = true;
				tag = "Launch Ball";
				////
			} else {
				Debug.Log (MechanicsManager.bounceProbability);
				Debug.Log (randomInt);
			}
		}
	}

	void SetStartingValues()
	{
		IncrementMultiplier ();	
		readyOnStart = true;
		returned = true;
		tag = "Launch Ball";
		gameObject.layer = 9;
		gameObject.GetComponent<Animator> ().SetBool ("isReady", true);
	}

}
