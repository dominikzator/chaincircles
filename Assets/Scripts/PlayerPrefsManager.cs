﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour {

	const string GAME_INITIATED = "game_initiated";
	const string TOTAL_SCORE_KEY = "total_score";
	const string HIGHEST_SCORE_KEY = "highest_score";
	const string CURRENT_BALL_INDEX = "current_ball_index";

	const string STANDARD_BALL_LOCK = "standard_ball_lock";
	const string GOLD_BALL_LOCK = "gold_ball_lock";
	const string SKY_BALL_LOCK = "sky_ball_lock";
	const string CIRCLE_BALL_LOCK = "circle_ball_lock";
	const string WAVE_BALL_LOCK = "wave_ball_lock";
	const string CORE_BALL_LOCK = "core_ball_lock";

	const string STANDARD_BALL_LEVEL = "standard_ball_level";
	const string GOLD_BALL_LEVEL = "gold_ball_level";
	const string SKY_BALL_LEVEL = "sky_ball_level";
	const string CIRCLE_BALL_LEVEL = "circle_ball_level";
	const string WAVE_BALL_LEVEL = "wave_ball_level";
	const string CORE_BALL_LEVEL = "core_ball_level";

	public static string[] locks = {STANDARD_BALL_LOCK,
									GOLD_BALL_LOCK,
									SKY_BALL_LOCK,
									CIRCLE_BALL_LOCK,
									WAVE_BALL_LOCK,
									CORE_BALL_LOCK};
	public static string[] levels = { STANDARD_BALL_LEVEL, 
									GOLD_BALL_LEVEL,
									SKY_BALL_LEVEL,
									CIRCLE_BALL_LEVEL,
									WAVE_BALL_LEVEL,
									CORE_BALL_LEVEL};

	void Awake()
	{
		Debug.Log ("AWAKEEE");
	}

	public static void SetBallLevel(int index, int value)
	{
		PlayerPrefs.SetInt (levels[index], value);
	}

	public static int GetBallLevel(int index)
	{
		return PlayerPrefs.GetInt( levels[index]);
	}

	public static void SetGameInitiated(int value)
	{
		PlayerPrefs.SetInt (GAME_INITIATED, value);
	}

	public static int GetGameInitiated()
	{
		return PlayerPrefs.GetInt( GAME_INITIATED);
	}

	public static void SetBallLock(int index, string value)
	{
		//locks [index] = value;
		PlayerPrefs.SetString (locks [index], value);
	}

	public static string GetBallLock(int index)
	{
			return PlayerPrefs.GetString( locks [index]);
	}

	public static void SetTotalScore(int score)
	{
		PlayerPrefs.SetInt (TOTAL_SCORE_KEY, score);
	}
	public static int GetTotalScore()
	{
		return PlayerPrefs.GetInt (TOTAL_SCORE_KEY);
	}
	public static void IncrementTotalScore(int value)
	{
		PlayerPrefs.SetInt (TOTAL_SCORE_KEY, PlayerPrefs.GetInt (TOTAL_SCORE_KEY) + value);
	}

	public static void SetCurrentBallIndex(int index)
	{
		PlayerPrefs.SetInt (CURRENT_BALL_INDEX, index);
	}
	public static int GetCurrentBallIndex()
	{
		return PlayerPrefs.GetInt (CURRENT_BALL_INDEX);
	}

	public static void SetHighestScore(int input)
	{
		PlayerPrefs.SetInt (HIGHEST_SCORE_KEY, input);
	}
	public static int GetHighestScore()
	{
		return PlayerPrefs.GetInt (HIGHEST_SCORE_KEY);
	}
}
