﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingX : MonoBehaviour {

	// Use this for initialization
	void Start () {
		float width = Screen.width;
		float height = Screen.height;
		float factor = width / height;

		transform.localPosition = new Vector3 (-10f * factor, 18f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
