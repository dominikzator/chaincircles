﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWave : MonoBehaviour {

	public SpriteRenderer spriteRend;
	public float lifeTime;
    public int hitNumber = 1;

    private float growSpeed = 2f;
	private float fadeOutSpeed = 1f;
	private float startingTime;
	private Vector3 startingScale;
	private bool removeflag = false;

	public bool triggerReset;

	private MechanicsManager mechanicsManager;
	private GameObject deathWavesParent;
	private DeathWavesManager deathWavesManager;

	// Use this for initialization
	void Start () {
		startingTime = Time.time;
		startingScale = new Vector3 (1f, 1f, 1f);
		mechanicsManager = FindObjectOfType<MechanicsManager>();
		deathWavesParent = GameObject.FindGameObjectWithTag ("DeathWavesParent");
		deathWavesManager = deathWavesParent.GetComponent<DeathWavesManager> ();
    }
	
	// Update is called once per frame
	void Update () {

		if (gameObject.transform.localScale.x < 4) {
			gameObject.transform.localScale += 
			new Vector3 (growSpeed * Time.deltaTime, 
				growSpeed * Time.deltaTime, 
				growSpeed * Time.deltaTime);
		} 
		else {
			
			spriteRend.color 
			= new Color (spriteRend.color.r, spriteRend.color.g, spriteRend.color.b, spriteRend.color.a - fadeOutSpeed*Time.deltaTime);
			if (spriteRend.color.a <= 0) {
				if (!removeflag) {
					removeflag = true;
					gameObject.transform.position = new Vector3 (-10f, 0f, 0f);
					mechanicsManager.waves.Remove (gameObject);
					gameObject.transform.SetParent (deathWavesParent.transform);
					deathWavesManager.deathWaves.Add (gameObject);
				}
			}
		}
		//if (Time.time - startTime > lifeTime) {
		//	Destroy (gameObject);
		//}
		if (triggerReset) {
			triggerReset = false;
			gameObject.transform.localScale = startingScale;
			spriteRend.color 
			= new Color (spriteRend.color.r, spriteRend.color.g, spriteRend.color.b, 1f);
			removeflag = false;
		}

	}


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.GetComponent<Circle> ()) {
			col.GetComponent<Circle> ().OnHitCircle (hitNumber);
		}
	}

}
