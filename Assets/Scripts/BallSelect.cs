﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BallSelect : MonoBehaviour {

	public MechanicsManager mechanicsManager;
	public static Sprite selectedBall;
	public Sprite[] ballSprites;
	private Text ballText;

	public static int ballIterator;

	// Use this for initialization
	void Start () {
		ballText = gameObject.GetComponentInChildren<Text> ();
		ballIterator = PlayerPrefsManager.GetCurrentBallIndex ();
		gameObject.GetComponent<Image> ().sprite = ballSprites [PlayerPrefsManager.GetCurrentBallIndex()];
		ballText.text = ballSprites [PlayerPrefsManager.GetCurrentBallIndex()].name;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ArrowLeft()
	{
		ballIterator--;
		if (ballIterator < 0) {
			ballIterator = ballSprites.Length - 1;
		}
		gameObject.GetComponent<Image> ().sprite = ballSprites [ballIterator];
		ballText.text = ballSprites [ballIterator].name;
		//Debug.Log (ballIterator);
		mechanicsManager.PriceUpdate();
		mechanicsManager.UpgradeUpdate ();
		mechanicsManager.SetTheBall ();
	}

	public void ArrowRight()
	{
		ballIterator++;
		if (ballIterator >= ballSprites.Length) {
			ballIterator = 0;
		}
		gameObject.GetComponent<Image> ().sprite = ballSprites [ballIterator];
		ballText.text = ballSprites [ballIterator].name;
		//Debug.Log (ballIterator);
		mechanicsManager.PriceUpdate();
		mechanicsManager.UpgradeUpdate ();
		mechanicsManager.SetTheBall ();

	}

	public void LoadMenuAndSetTheBall()
	{
		mechanicsManager.HomeButton ();
	}

}
