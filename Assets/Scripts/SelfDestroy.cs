﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestroy : MonoBehaviour {

	public float interval;

	private bool activated = false;
	private bool starttaken = false;
	private float startTime;

	private MechanicsManager mechanicsManager;

	// Use this for initialization
	void Start () {
		mechanicsManager = FindObjectOfType <MechanicsManager>();
		//Debug.Log (gameObject.name);
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (mechanicsManager.waves.Count);
		if (activated && !starttaken) {
			startTime = Time.time;
			starttaken = true;
		} 
		if (starttaken) {
			if (Time.time - startTime > interval) {
				//mechanicsManager.waves.RemoveAt (0);
				//mechanicsManager.waves.
				//Debug.Log("TEST");
				Destroy (gameObject);
			}	
		}
	}

	public void SelfDestroyActivated()
	{
		activated = true;
	}
}
