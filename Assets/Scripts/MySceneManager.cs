﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void HomeButton()
	{
		//Application.LoadLevel("01_Menu");
	}

	public void PauseGame()
	{
		GameObject home = GameObject.FindGameObjectWithTag ("Home Button");
		home.GetComponent<Image> ().enabled = true;
		home.GetComponent<Animator> ().SetBool ("gamePaused bool",true);


	}

}
