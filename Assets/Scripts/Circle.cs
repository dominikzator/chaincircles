﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Circle : MonoBehaviour {

	public GameObject deathWave, deathStars, coin;
	public Text frontText;
	public Animator anim;
	public SpriteRenderer rend;
	public ParticleSystem particle;
    public int hp;
    public int maxHp;
	public GameObject circlesCanvas;
	public GameObject deathWavesParent;
	public DeathWavesManager deathWavesManager;
	//private GameObject gameOverText;

	private AudioSource hitSound;
	private int addedvalue;
	private int addedvaluebuffor = 0;
    private bool dead;
	private bool gotStartingColor;
	private Color startingColor;
	private MechanicsManager mechanicsManager;
	private ParticleSystem.MainModule main;
	private Color col;

	void Awake()
	{
		dead = false;
		gotStartingColor = false;
		col = new Color(0f,0f,0f);
		main = particle.main;
	}

	// Use this for initialization
	void Start () {
		mechanicsManager = GameObject.FindGameObjectWithTag ("Mechanics Manager").GetComponent<MechanicsManager> ();
		circlesCanvas = GameObject.FindGameObjectWithTag ("Circles Canvas");
		deathWavesParent = GameObject.FindGameObjectWithTag ("DeathWavesParent");
		deathWavesManager = deathWavesParent.GetComponent<DeathWavesManager> ();
		//gameOverText = GameObject.FindGameObjectWithTag ("GameOver Text");
		hitSound = gameObject.GetComponent<AudioSource>();
		anim = GetComponentInParent<Animator> ();
		UpdateLife (hp);
		UpdateColor ();
        maxHp = hp;
		//SetHP (20);
	}

	private void UpdateLife(int value)
	{
		frontText.text = value.ToString ();
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		
        if(col.gameObject.tag == "Flying Ball")
        {
			int randomInt = Random.Range (0, 100);
			if (randomInt < MechanicsManager.doubleHitProbability) {
				Debug.Log (randomInt);
				OnHitCircle (2);
			} 
			else {
				OnHitCircle (1);	
			}
        }
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		//Lose Condition
		if (col.gameObject.name == "Trigger Border" && !MechanicsManager.gameover && mechanicsManager.loseConditionON) {
			//Debug.Log ("Lose Condition");
			mechanicsManager.GameOver();
			MechanicsManager.gameover = true;
			mechanicsManager.gameOverText.GetComponent<Text>().text = "You survived " + Spawner.waveNumber.ToString () + " waves";
			if (Spawner.waveNumber > PlayerPrefsManager.GetHighestScore ()) {
				PlayerPrefsManager.SetHighestScore (Spawner.waveNumber);
				mechanicsManager.UpdateHighestScore ();
			}
			addedvalue = Mathf.RoundToInt(Mathf.Floor (mechanicsManager.GetScoreFactor (Spawner.waveNumber) * Spawner.waveNumber));
			//Debug.Log ("Score Incremented: " + addedvalue);
			PlayerPrefsManager.SetTotalScore (PlayerPrefsManager.GetTotalScore ()+addedvalue);

			//mechanicsManager.gameOverText.gameObject.SetActive (true);
			mechanicsManager.gameOverText.gameObject.GetComponent<Text>().enabled = true;
			mechanicsManager.addedCoinsText.GetComponent<Text> ().text = "+" + addedvalue.ToString ();
			mechanicsManager.addedCoinsText.GetComponent<Text> ().enabled = true;

			//Debug.Log (mechanicsManager.circlesParent.name);
			Rigidbody2D[] rigids = mechanicsManager.circlesParent.GetComponentsInChildren<Rigidbody2D> ();
			mechanicsManager.lowerBorder.GetComponent<BoxCollider2D> ().isTrigger = true;
			for (int i = 0; i < rigids.Length; i++) {
				rigids[i].bodyType = RigidbodyType2D.Dynamic;
			}
			InvokeRepeating ("DecrementAddedCoins",0f,0.2f);
		}
	}

	public void DecrementAddedCoins()
	{
		//Debug.Log ("Decrement Function");
		//Debug.Log (addedvaluebuffor);
		addedvaluebuffor++;
		mechanicsManager.addedCoinsText.GetComponent<Text> ().text = "+" + (addedvalue - addedvaluebuffor).ToString ();
		MechanicsManager.coinText.text = (int.Parse(MechanicsManager.coinText.text)+1).ToString();
		if (addedvaluebuffor >= addedvalue) {
			CancelInvoke ();
		}
	}

	public void OnHitCircle (int hit) //Optimize
	{
		if (hp > 0) {
			int randomInt = Random.Range (0, 100);
			if(randomInt<MechanicsManager.waveProbability)
			{
				CreateDeathWave (5);
				Debug.Log ("WaveProb " + randomInt);
			}
			//AudioSource.PlayClipAtPoint (GetComponent<AudioSource> ().clip, transform.localPosition);
			//hitSound.
			hitSound.PlayOneShot(hitSound.clip,1f);
			randomInt = Random.Range (0, 100);
			if(randomInt<MechanicsManager.instantDestroyProbability)
			{
				hp = 0;
				Debug.Log ("CoreProb " + randomInt);
			}
			else{
			hp= hp - hit;
            if (hp < 0) hp = 0;
			}
			UpdateColor ();
			UpdateLife (hp);
		
		}
		if(hp<=0 && !dead){
			int randomInt = Random.Range (0, 100);

			if(randomInt<MechanicsManager.coinsProbability)
			{
			GameObject c = Instantiate (coin, transform.position, Quaternion.identity) as GameObject;
			c.transform.SetParent (MechanicsManager.circlesCanvas.transform);
			MechanicsManager.coinsBuffor++;
			//Debug.Log ("INT: " + randomInt);
			//Debug.Log ("PROB: " + MechanicsManager.coinsProbability);
			}

			dead = true;
			Destroy (GetComponent<CircleCollider2D>());
			CreateDeathWave (1);
			CreateDeathStars ();
			anim.SetTrigger ("deathTrigger");
		}
	}

	void CreateDeathWave (int hitValue)
	{
		//GameObject wave = Instantiate (deathWave, transform.position, Quaternion.identity) as GameObject;
		GameObject wave = deathWavesManager.GetFreeWave ();
		if (!wave) {
			//Debug.Log ("No free waves");
			//Debug.Log ("Instantiating wave");
			wave = Instantiate (deathWave, transform.position, Quaternion.identity) as GameObject;
		}
		deathWavesManager.deathWaves.Remove (wave);

		mechanicsManager.waves.Add (wave);
		wave.transform.SetParent (circlesCanvas.transform, false);
		wave.transform.position = gameObject.transform.position;
		wave.GetComponent<DeathWave> ().triggerReset = true;
		wave.gameObject.GetComponentInChildren<SpriteRenderer> ().color = startingColor;
		wave.GetComponentInChildren<DeathWave>().hitNumber = hitValue;
        if(maxHp>=20)
        {
            int lvl = maxHp / 10;
            wave.GetComponentInChildren<DeathWave>().hitNumber = lvl;
        }
	}

	void CreateDeathStars ()
	{
		GameObject stars = Instantiate (deathStars, transform.position, Quaternion.identity) as GameObject;
		stars.transform.SetParent (gameObject.transform.parent, false);
		stars.transform.localPosition = Vector3.zero;
	}


	public void DestroyCircle()
	{
		
		transform.parent.gameObject.GetComponent<SelfDestroy> ().SelfDestroyActivated ();
		Destroy (gameObject);
	}

	public void UpdateColor()
	{
		if(hp>6*MechanicsManager.colorRange)			//static White(the strongest)
		{
			col = new Color (1f, 1f, 1f,1f);
			rend.color = col;
			main.startColor = col;
		}
		if(hp<=6*MechanicsManager.colorRange && hp>5*MechanicsManager.colorRange)			//Lazur-White range
		{
			col = new Color (MechanicsManager.colorMulti*(hp-5*MechanicsManager.colorRange), 1f, 1f,1f);
			rend.color = col;
			main.startColor = col;
		}
		else if(hp<=5*MechanicsManager.colorRange && hp>4*MechanicsManager.colorRange)			//Blue-Lazur range
		{
			col = new Color (0f, MechanicsManager.colorMulti*(hp-4*MechanicsManager.colorRange), 1f,1f);
			rend.color = col;
			main.startColor = col;
		}
		else if(hp<=4*MechanicsManager.colorRange && hp>3*MechanicsManager.colorRange)			//Purple-Blue range
		{
			col = new Color (1f-MechanicsManager.colorMulti*(hp-3*MechanicsManager.colorRange), 0f, 1f,1f);
			rend.color = col;
			main.startColor = col;
		}
		else if(hp<=3*MechanicsManager.colorRange && hp>2*MechanicsManager.colorRange)	//Red-Purple range
		{
			col = new Color (1f, 0f, (MechanicsManager.colorMulti*(hp-2*MechanicsManager.colorRange)),1f);
			rend.color = col;
			main.startColor = col;
		}
		else if(hp<=2*MechanicsManager.colorRange && hp>MechanicsManager.colorRange)	//Yellow-Red range
		{
			col = new Color (1f, MechanicsManager.colorMulti*(2*MechanicsManager.colorRange-hp), rend.color.b,1f);
			rend.color = col;
			main.startColor = col;
		}
		else if(hp<=MechanicsManager.colorRange && hp>=0)	//Green-Yellow range
		{
			col = new Color (1f-MechanicsManager.colorMulti*(MechanicsManager.colorRange-hp), 1f, rend.color.b,1f);
			rend.color = col;
			main.startColor = col;
		}

		if(!gotStartingColor)
		{
			startingColor = col;
			gotStartingColor = true;
		}
	}

	public void SetHP(int input)
	{
		hp = input;
		UpdateLife (hp);
		UpdateColor ();
	}
}
