﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void DestroyCoin()
	{
		Destroy (gameObject);
	}

	public static void IncrementCoinsValue()
	{
		PlayerPrefsManager.IncrementTotalScore (1);
	}
	public static void SetInterfaceTotalCoins()
	{
		MechanicsManager.coinText.text = PlayerPrefsManager.GetTotalScore ().ToString ();
		MechanicsManager.coinText.GetComponent<Animator>().SetTrigger ("Coin Text Trigger");

	}


}
