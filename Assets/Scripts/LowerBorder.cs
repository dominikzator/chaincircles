﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowerBorder : MonoBehaviour {

	public GameObject firstBall;

	private Text gameOverText;
	private MechanicsManager mechanicsManager;

	// Use this for initialization
	void Start () {
		firstBall = GameObject.FindGameObjectWithTag ("Launch Ball");
		mechanicsManager = GameObject.FindGameObjectWithTag ("Mechanics Manager").GetComponent<MechanicsManager> ();

		gameOverText = GameObject.FindGameObjectWithTag ("GameOver Text").GetComponent<Text> ();

		//gameOverText.gameObject.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		
	}
}
