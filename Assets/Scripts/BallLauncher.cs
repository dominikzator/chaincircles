﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallLauncher : MonoBehaviour {

	public static int launchBallsNumber;
	public static bool launchRequested = false;

	public GameObject[] balls;
	public static List<GameObject> flyingBalls;
	public GameObject ball, dragRayElement, bubblesParent;
	public LowerBorder lowerBorder;
	public Vector3 force;
	public bool dragFlag;
	public int bubblesNumber;	
	public float forceMagnitude;
	public float launchInterval;

	private List <GameObject> bubbles;
	private MechanicsManager mechanicsManager;
	private Spawner spawner;
	private Vector3 dragStart,dragEnd, dragVector,currentballPosition;
	private int ballsIterator;
	private Rigidbody2D rgCurrentBall;
	public bool clickFlag;

	void Awake()
	{
		Time.timeScale = 1f;
		dragFlag = false;
		ballsIterator = 0;
		clickFlag = false;
	}

	void Update()
	{
		//Debug.Log (flyingBalls.Count);
	}

	// Use this for initialization
	void Start () {
		
		mechanicsManager = GameObject.FindGameObjectWithTag ("Mechanics Manager").GetComponent<MechanicsManager>();
		spawner = GameObject.FindGameObjectWithTag ("Spawner").GetComponent<Spawner>();
		balls = GameObject.FindGameObjectsWithTag ("Launch Ball");
		flyingBalls = new List<GameObject> ();
		launchBallsNumber = balls.Length;
	}

	void FixedUpdate () {
		if (launchRequested) {
			launchRequested = false;
			InvokeRepeating ("LaunchBall", 0f, 0.1f);
		}
	}

	public void ClickPressed()
	{
		if (spawner.spawnerSet 
			&& dragFlag
			&& !clickFlag
			&& !MechanicsManager.gameover 
			&& !MechanicsManager.gamePaused
			&& Ball.readyOnStart) {
			clickFlag = true;
			dragStart = Input.mousePosition;
			dragEnd = Input.mousePosition;
			bubbles = new List<GameObject> ();

			for (int i = 0; i < bubblesNumber; i++) {
				GameObject bubble = Instantiate (dragRayElement, lowerBorder.firstBall.transform.position, Quaternion.identity) as GameObject;
				bubble.transform.SetParent (bubblesParent.transform, false);
				bubbles.Add (bubble);
			}
		}
	}

	public void ClickReleased()
	{
		//Debug.Log ("Click Released");
		if (spawner.spawnerSet 
			&& dragFlag  
			&& dragVector.y > 0 
			&& !MechanicsManager.gameover 
			&& !MechanicsManager.gamePaused
			&& clickFlag
			&& dragStart !=dragEnd
			&& Ball.readyOnStart) {
			spawner.spawnerSet = false;
			currentballPosition = lowerBorder.firstBall.transform.position;

			if (Ball.multiplier > balls.Length) {
				for (int i = 0; i < Ball.multiplier - balls.Length; i++) {
					GameObject newBall = Instantiate (ball, currentballPosition, Quaternion.identity) as GameObject;
					newBall.transform.SetParent (mechanicsManager.ballsParent.transform);
					newBall.layer = 11;
					newBall.tag = "Launch Ball";
				}
			}
			balls = GameObject.FindGameObjectsWithTag ("Launch Ball");
			launchBallsNumber = balls.Length;
			lowerBorder.firstBall = null;
			dragFlag = false;
			ballsIterator = 0;

			launchRequested = true;
		} 
		else if (spawner.spawnerSet && dragVector.y <= 0 && clickFlag) {
			
		}

		clickFlag = false;
		DeleteBubbles ();
	}

	public void OnDrag()
	{
		if (spawner.spawnerSet 
			&& dragFlag 
			&& !MechanicsManager.gameover 
			&& !MechanicsManager.gamePaused
			&& clickFlag
			&& Ball.readyOnStart) {
			dragEnd = Input.mousePosition;

			dragVector = dragEnd - dragStart;

			dragVector = new Vector3 (-1 * dragVector.x, -1 * dragVector.y, -1 * dragVector.z);

			float a = (dragEnd.y - dragStart.y) / (dragEnd.x - dragStart.x);
			float bubbleX,bubbleY;
			float radius = 1f;
			float offset = dragVector.magnitude / 1000;
			bubbleY = 0;
			for (int i = 0; i < bubblesNumber; i++) {
				if (a > 0) {
					bubbleX = Mathf.Sqrt (radius * radius / (a * a + 1)); 
				} else {
					bubbleX = -1 * Mathf.Sqrt (radius * radius / (a * a + 1)); 
				}
				if (Mathf.Abs (a) != Mathf.Infinity) {
					bubbleY = a * bubbleX + lowerBorder.firstBall.transform.position.y;
				} 

				if (dragVector.y > 0 && Mathf.Abs(a) != Mathf.Infinity) {
					SetBubbles (true);
					bubbles [i].transform.position = new Vector3 (bubbleX + lowerBorder.firstBall.transform.position.x, bubbleY, 0f);
				} else {
					SetBubbles (false);
				}
				radius += offset;
			}
			Debug.DrawRay (lowerBorder.firstBall.transform.position, dragVector, Color.green);
		}
	}

	void SetBubbles(bool logic)
	{
		bubblesParent.SetActive(logic);
	}

	void DeleteBubbles()
	{
		for (int i = 0; i < bubbles.Count; i++) {
			//if(bubbles[i])
			Destroy (bubbles [i]);
		}
	}

	public void LaunchBall() //optimize
	{
		//Debug.Log("launch");
		BallLauncher.flyingBalls.Add(balls[ballsIterator]);
		rgCurrentBall = balls[ballsIterator].GetComponent<Rigidbody2D>();
		rgCurrentBall.bodyType = RigidbodyType2D.Dynamic;
		//if (Ball.freezeX) {
		//	rgCurrentBall.constraints = RigidbodyConstraints2D.FreezePositionX;
		//}
		force = dragVector * Time.deltaTime;
		float scaleDragVectorFactor = forceMagnitude / force.magnitude;
		force = force * scaleDragVectorFactor;
		
		
		balls[ballsIterator].tag = "Flying Ball";
		balls[ballsIterator].layer = 11;
		ballsIterator++;
		//int displayMultiplier = Ball.multiplier-ballsIterator;
		if (balls.Length == ballsIterator) {
			CancelInvoke ();
		}
		
		rgCurrentBall.AddForce (force);
	}
}
